# Super Simple Load More
### By Anthony Coffey
###### If you have any questions, please feel free to contact me anytime!
###### e-mail support: coffey.j.anthony@gmail.com

### Code Features

- Shortcode with 11 possible parameters
- Hassle-free, modular solution 
- Use the shortcode unlimited times, not limited to one use per page

### Please review the code changes made to the following files:
###### You can refer to this repository to review the code that I developed.

- functions.php
- taxonomy-resource-topic.php

### Shortcode

`[supersimple_load_more post_type="post" article_class="fourPer" posts_per_page="4" page="2"]`

### Shortcode Options

| Option | Default Value | Description |Datatype|
| :---: | :---: | :--- |:---:|
| `article_class` | `fourPer` | Assigns a class to the individual `<article>` tags. |string|
| `button_class` | `super-simple-load-more` | Assigns a class to the "Load More" anchor tag. |string|
| `button_id` | Random number | assigns a __unique__ ID attribute to the "Load More" anchor tag. |string/int |
| `image_class` | | Assigns a class to individual post images. |string|
| `post_type` | `post` | Sets the post type(s) to be used for the query | array, string |
| `posts_per_page` | `10` | Sets the number of posts retrieved with each query. |int|
| `page` | `1` | Used to identify which page of the queried results to retrieve. |int|
|__Taxonomy Filters__|| ___Accepts comma separated list of term id's OR slugs___  ||
| `resource_type` |  | Used for targeting specific posts in the `resource_type` taxonomy. |string, int|
| `resource_topic` |  | Used for targeting specific posts in the `resource_topic` taxonomy. |string, int|
| `resource_series` |  | Used for targeting specific posts in the `resource_series` taxonomy. |string, int|
| `category` |  | Used for targeting specific posts in the `category` taxonomy. |string, int|
| `post_tag` |  | Used for targeting specific posts in the `post_tag` taxonomy. |string, int|
| `orderby`  | `title` | Sets `orderby` for WP Query, see codex for available options. Defaults to "title" | string |
| `order`    | `ASC`   | Sets `order` for WP Query, can be either `ASC` or `DESC` | string |
| `exlude_posts` | | Excludes posts from the query by ID, useful for excluding posts that are already displayed. Accepts comma separated string of Post ID's | string |
- ___NOTE:___ You can query multiple different taxonomy simultaneously

