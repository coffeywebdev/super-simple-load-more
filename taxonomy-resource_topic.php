<?php
$resourceCount = 0;
if (have_posts()) : ?>

<div class="resource-type-wrapper type-songs">
  <h2>Songs</h2>
  <?php
    // declare array for holding Post ID's
    $displayed_posts = array();
    while (have_posts()) : the_post();
      if ($post->post_type == 'song') :
        if($resourceCount < 4):
          $displayed_posts[] = $post->ID;
          include(locate_template('template-resourceblock-songs.php'));
          $resourceCount++;
        endif;
      endif;
    endwhile;
    // turn array of post ID's into string for passing to shortcode
    $displayed_posts = implode(',', $displayed_posts);
    // INSERT LOAD MORE SHORTCODE HERE
    echo do_shortcode("[supersimple_load_more post_type='song' exclude_posts='{$displayed_posts}' resource_topic='{$term_slug}' posts_per_page='4' image_class='allsongsthumb' orderby='title' order='ASC']");
  ?>
</div>

<?php endif; ?>


<?php rewind_posts(); ?>



<?php
$resourceCount = 0;
if (have_posts()) : ?>

<div class="resource-type-wrapper type-wkshts">
  <h2>Flashcards</h2>
  <?php $hold_post_type; ?>
  <?php while (have_posts()) : the_post(); $hold_post_type = $post->post_type;
    if( has_term( 'flashcards', 'resource_type') ) :
      if($resourceCount<4){
        include(locate_template('template-resourceblock.php'));
        $resourceCount++;
      }
    endif;
  endwhile;
    echo do_shortcode("[supersimple_load_more post_type='{$hold_post_type}' resource_type='flashcards' resource_topic='{$term_slug}' posts_per_page='4' page='2' image_class='']");
  // INSERT LOAD MORE SHORTCODE HERE

  ?>
</div>

<?php endif; ?>

<?php rewind_posts(); ?>
