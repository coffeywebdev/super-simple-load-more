<?php
/* DO NOT COPY ABOVE THIS LINE WHEN PASTING INTO FUNCTIONS.PHP */


/* BEGIN PASTING HERE */

/*
    Super Simple Ajax Load More by Anthony Coffey
    e-mail supprt: coffey.j.anthony@gmail.com
*/
function supersimple_ajax_load_more_button($atts){
  // shortcode parameters and defaults
  $a = shortcode_atts( array(
    'post_type'        => 'post',
    'article_class'    => 'fourPer',
    'button_class'    => 'super-simple-load-more',
    'button_id'       => rand(99,999),
    'image_class'     => '',
    'posts_per_page'  => 10,
    'page'           => 1,
    'resource_type'   => '',
    'resource_topic'  => '',
    'resource_series' => '',
    'category'        => '',
    'post_tag'        => '',
    'orderby'        => '',
    'order'        => '',
	  'exclude_posts' => array()
  ), $atts );
  ob_start();
  ?>
  <a
    id="ssalm-<?= $a['button_id'] ?>"
    class="<?= $a['button_class']; ?>"
    data-page="<?= $a['page']; ?>"
  >Show More</a>
  <script type="text/javascript">
    jQuery(document).ready( function() {

      jQuery('#ssalm-<?= $a['button_id']; ?>').on( 'click', function() {
        var shortcode_data = jQuery(this);
        jQuery('.load-more-error').remove();
        jQuery(this).parent().append('<div id="loading-<?= $a['button_id']; ?>" class="ssalm-loading"></div>');
        supersimple_ajax_load_more(shortcode_data);
      } );

      function supersimple_ajax_load_more(shortcode_data) {
        var data = {
          action: 'supersimple_ajax_load_more',
          post_type: <?= json_encode( $a['post_type'] ); ?>,
          posts_per_page: <?= $a['posts_per_page']; ?>,
          page: shortcode_data.data('page'),
          article_class: <?= json_encode( $a['article_class'] ); ?>,
          image_class: <?= json_encode( $a['image_class'] ); ?>,
          resource_type: <?= json_encode( $a['resource_type'] ); ?>,
          resource_topic: <?= json_encode( $a['resource_topic'] ); ?>,
          resource_series: <?= json_encode( $a['resource_series'] ); ?>,
          category: <?= json_encode( $a['category'] ); ?>,
          post_tag: <?= json_encode( $a['post_tag'] ); ?>,
          orderby: <?= json_encode( $a['orderby'] ); ?>,
          order: <?= json_encode( $a['order'] ); ?>,
          post__not_in: <?= json_encode( $a['exclude_posts'] ); ?>
        }

        jQuery.ajax({
          url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
          type: 'post',
          data: data,
          success: function (response) {
            // parse response
            response = JSON.parse(response);
            console.log(response);
            // add posts to parent div
            var parent_div = jQuery('#ssalm-<?= $a['button_id']; ?>').parent();
            jQuery('#loading-<?= $a['button_id']; ?>').remove()
            jQuery('#ssalm-<?= $a['button_id']; ?>').parent().append(response.html);
            var page = jQuery('#ssalm-<?= $a['button_id']; ?>').data('page');
            jQuery('#ssalm-<?= $a['button_id']; ?>').data('page', page + 1);

            // if max_num_pages is not equal to the current page, move the load more button below the queried posts
            if(response.max_num_pages != response.current_page){
              jQuery('#ssalm-<?= $a['button_id']; ?>').appendTo(parent_div);
            } else {
            //  if max_num_pages is equal to current page, remove the load more button
              jQuery('#ssalm-<?= $a['button_id']; ?>').remove();
            }

          }
        });
      }

    });
  </script>
  <?php
  return ob_get_clean();
}
add_shortcode('supersimple_load_more', 'supersimple_ajax_load_more_button');

/* advanced ajax load more function */
function supersimple_ajax_load_more(){
  $html = '';
  $a = $_POST;
  $image_class = $a['image_class'];
  // default query arguments
  $args = array(
    'post_type'       => explode(',', $a['post_type']),
    'post_status'     => array('published'),
    'orderby'         => ($a['orderby']) ? $a['orderby'] : 'title',
    'order'           => ($a['order']) ? $a['order'] : 'ASC',
  );

  // build WP Query arguments variable
  $query_args = array(
    'post_type'           => $args['post_type'],
    'post_status'         => $args['post_status'],
    'ignore_sticky_posts' => 1,
    'posts_per_page'      => $a['posts_per_page'],
    'paged'               => $a['page'],
    'orderby'             => $args['orderby'],
    'order'               => $args['order'],
    'tax_query'           => array(),
    'meta_query'          => array(),
    'post__not_in'        => explode(',',$a['post__not_in'])
  );

  // use "OR" for taxonomy queries (instead of "AND")
  $query_args['tax_query']  = array( 'relation' => 'AND' );

  // resource_type
  if(!empty($a['resource_type'])){
    $resource_types = explode(',', $a['resource_type'] );
    $field = is_numeric( $resource_types[0] ) ? 'term_id' : 'slug';
    $query_args['tax_query'][] = array(
      'taxonomy' => 'resource_type',
      'field' => $field,
      'operator' => 'IN',
      'terms' => $resource_types
    );
  }

  // resource_topic
  if(!empty($a['resource_topic'])){
    $resource_topics = explode(',', $a['resource_topic'] );
    $field = is_numeric( $resource_topics[0] ) ? 'term_id' : 'slug';
    $query_args['tax_query'][] = array(
      'taxonomy' => 'resource_topic',
      'field' => $field,
      'operator' => 'IN',
      'terms' => $resource_topics
    );
  }

  // resource_series
  if(!empty($a['resource_series'])){
    $resource_series = explode(',', $a['resource_series'] );
    $field = is_numeric( $resource_series[0] ) ? 'term_id' : 'slug';
    $query_args['tax_query'][] = array(
      'taxonomy' => 'resource_series',
      'field' => $field,
      'operator' => 'IN',
      'terms' => $resource_series
    );
  }

  // category
  if(!empty($a['category'])){
    $categories = explode(',', $a['category'] );
    $field  = is_numeric( $categories[0] ) ? 'term_id' : 'slug';
    $query_args['tax_query'][] = array(
      'taxonomy' => 'category',
      'field' => $field,
      'operator' => 'IN',
      'terms' => $categories
    );
  }

  // post_tag
  if(!empty($a['post_tag'])){
    $tags = explode(',', $a['post_tag'] );
    $field = is_numeric( $tags[0] ) ? 'term_id' : 'slug';
    $query_args['tax_query'][] = array(
      'taxonomy' => 'post_tag',
      'field' => $field,
      'operator' => 'IN',
      'terms' => $tags
    );
  }

  // if no taxonomy queried, unset tax_query value
  if ( empty( $query_args['tax_query'] ) ) {
    unset( $query_args['tax_query'] );
  }

  $query = new WP_Query($query_args);

  $html = "";

  if($query->have_posts()):
    while($query->have_posts()) : $query->the_post();
      $post_id = get_the_ID();
      $article_class = $a['article_class'];
      $post_url = get_the_permalink();
      $post_title = get_the_title();
      $post_image_url = get_the_post_thumbnail_url();
      $html .= "<article id='post-{$post_id}' role='article' class='{$article_class}'>
                <header class='article-header'>
                  <div class='thumbWrapper'>
                    <a href='{$post_url}' rel='bookmark' title='{$post_title}'>
                      <img src='{$post_image_url}' class='{$image_class} wp-post-image' alt='{$post_title}' data-lazy-loaded='true'  width='150' height='150'/>
                      </a>
                  </div>
                  <h3 class='h2'><a href='{$post_url}' rel='bookmark' title='{$post_title}'>{$post_title}</a></h3>
                </header>
              </article>";
    endwhile;
  else:
    // could display "no results found" message here
    // $html .= "<p class='load-more-error'>No results found...</p>";
  endif;
  // clear row
  $html .= "<div style='clear:both;'></div>";
  // reset query
  wp_reset_query();

  $response = array('success'=>true,'html'=>$html, 'query'=>$query->query, 'post_count'=>$query->post_count, 'max_num_pages'=>$query->max_num_pages,'current_page'=> (int) $query->query['paged']);
  echo json_encode($response);
  wp_die();
}
add_action( 'wp_ajax_supersimple_ajax_load_more', 'supersimple_ajax_load_more' );
add_action( 'wp_ajax_nopriv_supersimple_ajax_load_more', 'supersimple_ajax_load_more' );
/* END SUPER SIMPLE AJAX CODE */


/* DO NOT COPY BELOW THIS LINE */
?>
